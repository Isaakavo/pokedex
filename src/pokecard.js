import {
  LitElement,
  html,
  css,
} from '../node_modules/lit-element/lit-element.js';

import { pokecardStyles } from '../styles/pokecardStyles.js';

export class Pokecard extends LitElement {
  static get styles() {
    return [
      css`
        :host {
          text-transform: capitalize;
        }
      `,
      pokecardStyles,
    ];
  }

  static get properties() {
    return {
      sprites: { type: Object },
      name: { type: String },
      pokemonTypes: { type: Array, noAccesor: true },
      height: { type: Number },
      weight: { type: Number },
      url: { type: String, noAccesor: true },
      colors: { type: Array },
      clickable: { type: Boolean },
      //The use of attribute to change to url doesn't work
      //The effective way of update the template is using custom accessor and
      //call get pokemon.
      // url: { type: String, reflect: true, attribute: 'poke-url' },
      id: { type: Number },
    };
  }

  setBackgroundCardColorGradient(first, second) {
    this.style.setProperty('--background', first);
    this.style.setProperty('--background-second', second);
  }

  constructor() {
    super();
    this.url = '';
    this.sprites = {};
    this.name = '';
    this.pokemonTypes = [];
    this.height = 0;
    this.weight = 0;
    this.colors = new Array();
    this.clickable = true;

    this.addEventListener('click', this._cardClick);
  }

  connectedCallback() {
    super.connectedCallback();
    this.getPokemon();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    this.removeEventListener('click', this._cardClick);
  }

  set url(val) {
    let oldVal = this._url;
    this._url = val;
    this.requestUpdate('url', oldVal).then((result) => {
      if (result) {
        this.getPokemon();
      }
    });
  }

  get url() {
    return this._url;
  }

  set pokemonTypes(val) {
    let oldVal = this._pokemonTypes;
    this._pokemonTypes = val;
    this.requestUpdate('pokemonTypes', oldVal).then((result) => {
      if (result) {
        this.putColorBackground();
      }
    });
  }

  get pokemonTypes() {
    return this._pokemonTypes;
  }

  async getPokemon() {
    try {
      let response = await fetch(this.url);
      if (response.ok) {
        const poke = await response.json();
        this.sprites = poke.sprites;
        this.name = poke.name;
        this.pokemonTypes = poke.types;
        this.height = poke.height;
        this.weight = poke.weight;
        this.id = poke.id;
        this.putColorBackground();
      } else {
        console.log(response.status);
      }
    } catch (err) {
      console.log(err);
    }
  }

  putColorBackground() {
    if (this.pokemonTypes.length === 2) {
      let colors = this.pokemonTypes.reduce((acc, obj) => {
        acc[obj.slot] = obj.type.name;
        return acc;
      }, []);
      colors.shift();
      let firstColor = this.colors[colors[0]];
      let secondColor = this.colors[colors[1]];
      this.setBackgroundCardColorGradient(firstColor, secondColor);
    } else {
      if (this.pokemonTypes[0] !== undefined) {
        this.setBackgroundCardColorGradient(
          this.colors[this.pokemonTypes[0].type.name],
          this.colors[this.pokemonTypes[0].type.name]
        );
      }
    }
  }

  render() {
    return html` <div
      class="elementContainer"
      @click=${this.clickable ? this._cardClick : ''}
    >
      <div class="${this.clickable ? 'cardPointer' : 'cardNoPointer'}">
        <p>${this.id}</p>
        <h2 class="pokeName">${this.name}</h2>
        <div class="pokeImage">
          <img
            src="${this.sprites.front_default}"
            alt="PokeImage"
            id="pokeImage"
          />
        </div>
        <div class="container">
          <div class="innerContainer">
            <div class="types">
              <ul class="typesList">
                <p>Type(s)</p>
                <li>
                  ${this.pokemonTypes.map((type) => {
                    return html`<li>${type.type.name}</li>`;
                  })}
                </li>
              </ul>
            </div>
            <div class="types">
              <ul class="typesList">
                <p>Stats</p>
                <li>
                  Height:
                  ${this.height >= 10
                    ? `${this.height / 10}m`
                    : `${this.height * 10}cm`}
                </li>
                <li>
                  Weight:
                  ${this.weight > 10
                    ? `${this.weight / 10}kg`
                    : `${this.weight * 10}g `}
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>`;
  }

  _cardClick(e) {
    if (e.target.name !== null) {
      this.dispatchEvent(
        new CustomEvent('on-click', {
          bubbles: true,
          composed: true,
          detail: e.target.name,
        })
      );
    }
  }
}
customElements.define('poke-card', Pokecard);
