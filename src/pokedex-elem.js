import {
  LitElement,
  html,
  css,
} from '../node_modules/lit-element/lit-element.js';
import './pokecard.js';
import './serach-bar.js';
import '@polymer/app-layout/app-layout.js';

import { pokedexStyles } from '../styles/pokedexStyles.js';

// function getPokemon(pokemonURL) {
//   return html`
//     <poke-card .url=${pokemonURL.url} .colors=${this.colors}></poke-card>
//   `;
// }

export class PokedexElem extends LitElement {
  static get styles() {
    return [
      css`
        :host {
          display: block;
          font-size: 18px;
        }
      `,
      pokedexStyles,
    ];
  }

  static get properties() {
    return {
      pokemons: { type: Array },
      pokemon: { type: Object },
      offset: { type: Number },
      listLimit: { type: Number },
      colors: { type: Array },
      search: { type: String },
      hasSearched: { type: Boolean },
      totalOfPokemons: { type: Number },
      page: { type: Number },
      pages: { type: Number },
    };
  }

  async getPokemons() {
    try {
      let response = await fetch(
        // `https://pokeapi.co/api/v2/pokemon/${this.index}`
        `https://pokeapi.co/api/v2/pokemon?limit=${this.listLimit}&offset=${this.offset}`
      );
      if (response.ok) {
        const poke = await response.json();
        this.pokemons = poke;
        this.totalOfPokemons = this.pokemons.count;

        this.pages = Math.ceil(this.totalOfPokemons / this.listLimit);
      } else {
        console.log(response.status);
      }
    } catch (err) {
      console.log(err);
    }
  }

  async getPokemonSearch() {
    try {
      let response = await fetch(
        `https://pokeapi.co/api/v2/pokemon/${this.search}`
      );
      if (response.ok) {
        const poke = await response.json();
        this.pokemon = poke;
        this.hasSearched = true;
      } else {
        alert('Couldnt find the pokemon!');
        console.log(response.status);
      }
    } catch (err) {
      console.log(err);
    }
  }

  async getColors() {
    try {
      const colorsResponse = await fetch('../colors.json');
      if (colorsResponse.ok) {
        let colors = await colorsResponse.json();
        this.colors = colors.reduce((acc, obj) => {
          acc[obj.type] = obj.color;
          return acc;
        }, []);
        this.getPokemons();
      } else {
        console.log(colorsResponse.status);
      }
    } catch (err) {
      console.error(err);
    }
  }

  connectedCallback() {
    super.connectedCallback();
    this.getColors();
  }

  disconnectedCallback() {
    this.removeEventListener('on-search', this._onSearch);
    this.removeEventListener('on-clear', this._onClear);
    this.removeEventListener('on-click', this._onCardClick);
  }

  constructor() {
    super();
    this.offset = 0;
    this.listLimit = 18;
    this.pokemon = {};
    this.pokemons = [];
    this.colors = [];
    this.hasSearched = false;
    this.search = '';
    this.totalOfPokemons = 0;
    this.page = 1;
    this.pages = 0;

    this.addEventListener('on-search', this._onSearch);
    this.addEventListener('on-clear', this._onClear);
    this.addEventListener('on-click', this._onCardClick);
  }

  render() {
    return html`
      <app-header style="background: #f6bd20; " condenses fixed>
        <app-toolbar>
          <img src="../img/poke-icon.png" alt="" style="width: 3%" />
          <search-bar></search-bar>
          ${this.buttonContainerComponent()}
        </app-toolbar>
      </app-header>
      <div class="${this.hasSearched ? 'containerBlock' : 'container'}">
        <div>
          <p>Page ${this.page} of ${this.pages}</p>
        </div>
        <div style="${this.page > 1 ? 'display: none' : ''}">
          <h2>We have ${this.totalOfPokemons} pokemons!</h2>
        </div>
        <div class="${this.hasSearched ? 'singleCard' : 'pokecardContainer'}">
          ${!this.hasSearched
            ? this.pokemons.results.map((pokemon) => {
                return html`
                  <poke-card
                    .url=${pokemon.url}
                    .colors=${this.colors}
                  ></poke-card>
                `;
              }) //this.pokemons.results.map(getPokemon)
            : html`<poke-card
                .sprites=${this.pokemon.sprites}
                .name=${this.pokemon.name}
                .pokemonTypes=${this.pokemon.types}
                .height=${this.pokemon.height}
                .weight=${this.pokemon.weight}
                .colors=${this.colors}
                .clickable=${false}
              ></poke-card>`}
        </div>
      </div>
    `;
  }

  _onNextPage() {
    this.offset += this.listLimit;
    this.pokemons = [];
    if (this.page < this.pages) {
      this.page++;
      this.getPokemons();
    }
  }

  _onPrevPage() {
    if (this.pokemons.previous !== null) {
      this.offset -= this.listLimit;
      this.page--;
      this.getPokemons();
    } else {
      alert('No prev pages');
    }
  }

  _onSearch(event) {
    const value = event.detail;

    if (value) {
      this.search = value.toLowerCase();

      this.getPokemonSearch();
    } else {
      this.hasSearched = false;
    }
  }

  _onClear() {
    this.hasSearched = false;
    this.search = '';
  }
  _onCardClick(e) {
    if (e.detail !== null) {
      console.log('Pokedex elem', e.detail);
      this.search = e.detail;
      this.getPokemonSearch();
    }
  }

  buttonContainerComponent() {
    return html` <div
      class="buttonContainer"
      style="${this.hasSearched ? 'display: none;' : ''}"
    >
      <div class="buttons">
        <button @click=${this._onPrevPage}>Prev page</button>
        <button @click=${this._onNextPage}>Next Page</button>
      </div>
    </div>`;
  }
}
customElements.define('pokedex-elem', PokedexElem);
