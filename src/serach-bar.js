import { LitElement, html, css } from 'lit-element';

export class SerachBar extends LitElement {
  static styles = css`
    :host {
      margin: 0 15px;
    }

    .search-bar {
      display: flex;
      box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2),
        0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
      border-radius: 5px 0 0 5px;
    }

    .search {
      width: 100%;
      border: 3px solid #e0e0e0;
      border-right: none;
      padding: 5px;
      height: 20px;
      outline: none;
      color: #000;
    }

    .search:focus {
      color: #000;
    }

    .buttonContainer {
      display: contents;
    }

    .searchButton {
      height: 36px;
      border: 1px solid #e0e0e0;
      background: #e0e0e0;
      text-align: center;
      cursor: pointer;
      font-size: 20px;
    }

    .searchClear {
      height: 36px;
      border: 1px solid #e0e0e0;
      background: #e0e0e0;
      text-align: center;
      cursor: pointer;
      font-size: 20px;
      border-radius: 0 5px 5px 0;
    }
  `;

  static get properties() {
    return {
      search: { type: String },
    };
  }

  constructor() {
    super();
    this.search = '';

    this.addEventListener('keyup', this.onEnterPress);
  }

  render() {
    return html`
      <div class="search-bar">
        <div>
          <input
            class="search"
            type="text"
            id="search"
            placeholder="Search Pokemon!"
            .value="${this.search}"
            @change="${this.updateSearch}"
          />
        </div>

        <div class="buttonContainer">
          <button
            id="search-btn"
            class="searchButton"
            @click="${this.onSearch}"
          >
            Search
          </button>
          <button id="clear-btn" class="searchClear" @click="${this.onClear}">
            Clear
          </button>
        </div>
      </div>
    `;
  }

  onSearch() {
    this.dispatchEvent(
      new CustomEvent('on-search', {
        bubbles: true,
        composed: true,
        detail: this.search,
      })
    );
  }

  onClear() {
    this.search = '';
    this.dispatchEvent(
      new CustomEvent('on-clear', {
        bubbles: true,
        composed: true,
      })
    );
  }

  updateSearch(event) {
    this.search = event.target.value;
  }

  onEnterPress(e) {
    if (e.keyCode === 13) {
      console.log(e.key);
      this.onSearch();
    }
  }
}
customElements.define('search-bar', SerachBar);
