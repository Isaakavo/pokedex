import { css } from '../node_modules/lit-element/lit-element.js';

export const pokedexStyles = css`
  h2 {
    text-align: center;
    margin: auto;
  }
  p {
    font-size: 1.3em;
    text-align: end;
    margin: 5px 50px auto;
  }
  .container {
    display: block;
    margin-bottom: 25px;
  }
  .containerBlock {
    display: block;
  }
  .buttonContainer {
    width: auto;
    margin-left: auto;
  }
  .buttons {
    text-align: center;
  }
  .pokecardContainer {
    /* columns: 5;
    column-gap: 5px;
    width: max-content; */
    display: grid;
    grid-template-columns: auto auto auto;
    grid-template-rows: auto auto auto auto auto;
    justify-content: center;
    grid-gap: 15px;
    margin-top: 15px;
  }
  .singleCard {
    width: 25%;
    margin-left: auto;
    margin-right: auto;
    margin-top: 15px;
    /* margin: 0;
      height: 100vh;
      display: flex;
      align-items: center;
      justify-content: center; */
    /* background: #f6bd20; */
    /* font-size: 50px; */
    /* font-weight: bold; */
    /* font-family: system-ui; */
  }
  button {
    background-color: #ddd;
    color: black;
    border: none;
    padding: 5px 5px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 1.1em;
    cursor: pointer;
    border-radius: 6px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    transition: 0.3s;
    /* margin-top: 25px;
    margin-bottom: 25px;
    padding-right: 15px;
    margin-left: 25px; */
  }
`;
