import { css } from '../node_modules/lit-element/lit-element.js';

export const pokecardStyles = css`
  .cardPointer {
    padding: 0.7em;
    /* background-color: var(--background); */
    background: var(--background);
    background: linear-gradient(
      45deg,
      var(--background) 30%,
      var(--background-second) 90%
    );
    border-radius: 6px;
    cursor: pointer;
  }
  .cardNoPointer {
    padding: 1.2em;
    /* background-color: var(--background); */
    background: var(--background);
    background: linear-gradient(
      45deg,
      var(--background) 30%,
      var(--background-second) 90%
    );
    border-radius: 6px;
  }
  .elementContainer:hover {
    box-shadow: 0 12px 16px 0 rgba(0, 0, 0, 0.6);
  }
  .elementContainer {
    /* margin: 5px; */
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    transition: 0.3s;
    border-radius: 6px;
  }
  .container {
    padding: 2px 8px;
    margin-top: 5px;
    border: solid rgb(33, 33, 33);
    /* background: rgb(255, 253, 221); */
    background: linear-gradient(
      45deg,
      rgb(193, 154, 107) 30%,
      rgb(255, 255, 255) 70%
    );
  }

  .pokeImage {
    width: auto;
    height: auto;
    text-align: center;
    background-image: url('../img/img-background.jpg');
    border: #000 #212121;
  }

  .pokeImage > img {
    width: 200px;
    height: 200px;
    margin-left: auto;
    margin-right: auto;
  }
  .innerContainer {
    /* display: flex;
    flex-direction: row; */
    display: grid;
    grid-template-columns: auto auto auto;
    grid-template-rows: auto;
    grid-row-gap: 5px;
    justify-content: space-evenly;
  }
  ul {
    list-style: none;
    padding: 0;
  }
  .types {
    margin-left: 5px;
    margin-right: 10px;
  }
  .typesList {
    margin-right: 15px;
  }
`;
